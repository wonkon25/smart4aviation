package com.was.konrad.services;

import com.was.konrad.interfaces.AirportServiceInterface;
import com.was.konrad.models.Baggage;
import com.was.konrad.models.Flight;
import com.was.konrad.models.Load;

import java.util.List;
import java.util.stream.Collectors;

import static com.was.konrad.converter.ConvertJsonToList.cargosToList;
import static com.was.konrad.converter.ConvertJsonToList.flightToList;

public class AirportService implements AirportServiceInterface {
    @Override
    public int numberOfFlightDeparting(String iataCode, String departureDate) {

        return (int) flightToList().stream()
                .filter(a -> a.getDepartureDate().contains(departureDate)
                        && a.getDepartureAirportIATACode().equals(iataCode))
                .count();
    }

    @Override
    public int numberOfFlightArriving(String iataCode, String departureDate) {

        return (int) flightToList().stream()
                .filter(a -> a.getDepartureDate().contains(departureDate)
                        && a.getArrivalAirportIATACode().equals(iataCode))
                .count();
    }

    @Override
    public int piecesOfBaggageDepartingFromThisAirport(String iataCode, String departureDate) {

        List<Flight> flightsId = flightToList().stream()
                .filter(a -> a.getDepartureDate().contains(departureDate)
                        && a.getDepartureAirportIATACode().equals(iataCode))
                .collect(Collectors.toList());

        List<Load> loadListByFlightDepartingId = cargosToList().stream()
                .filter(l -> flightsId.stream()
                        .anyMatch(i -> i.getFlightId().equals(l.getFlightId())))
                .collect(Collectors.toList());


        return loadListByFlightDepartingId.stream()
                .flatMap(x -> x.getBaggage().stream())
                .mapToInt(Baggage::getPieces)
                .sum();
    }

    @Override
    public int numberOfBaggageArrivingToThisAirport(String iataCode, String departureDate) {

        List<Flight> flightsId = flightToList().stream()
                .filter(a -> a.getDepartureDate().contains(departureDate)
                        && a.getArrivalAirportIATACode().equals(iataCode))
                .collect(Collectors.toList());

        List<Load> loadListByArrivingFlightId = cargosToList().stream()
                .filter(l -> flightsId.stream()
                        .anyMatch(i -> i.getFlightId().equals(l.getFlightId())))
                .collect(Collectors.toList());


        return loadListByArrivingFlightId.stream()
                .flatMap(x -> x.getBaggage().stream())
                .mapToInt(Baggage::getPieces)
                .sum();
    }
}