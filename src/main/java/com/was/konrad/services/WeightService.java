package com.was.konrad.services;

import com.was.konrad.interfaces.WeightServiceInterface;
import com.was.konrad.models.Baggage;
import com.was.konrad.models.Cargo;
import com.was.konrad.models.Flight;
import com.was.konrad.models.Load;

import java.util.List;
import java.util.stream.Collectors;

import static com.was.konrad.converter.ConvertJsonToList.cargosToList;
import static com.was.konrad.converter.ConvertJsonToList.flightToList;

public class WeightService implements WeightServiceInterface {
    @Override
    public int flightId(int flightNumber, String departureDate) {
        return flightToList().stream()
                .filter(a -> a.getFlightNumber().equals(flightNumber)
                        && a.getDepartureDate().contains(departureDate))
                .map(Flight::getFlightId)
                .findFirst()
                .get();
    }

    @Override
    public int baggageWeightFotRequestFlight(int id) {

        List<Load> loadList = cargosToList().stream()
                .filter(c -> c.getFlightId().equals(id))
                .collect(Collectors.toList());

        List<Baggage> baggageList = loadList.stream()
                .map(Load::getBaggage)
                .findAny()
                .get();

        return baggageList.stream()
                .filter(o -> o.getWeight() > 10)
                .mapToInt(Baggage::getWeight)
                .sum();
    }

    @Override
    public int cargoWeightFotRequestFlight(int id) {

        List<Load> loadList = cargosToList().stream()
                .filter(c -> c.getFlightId().equals(id))
                .collect(Collectors.toList());

        List<Cargo> cargoList = loadList.stream()
                .map(Load::getCargo)
                .findAny()
                .get();

        return cargoList.stream()
                .filter(o -> o.getWeight() > 10)
                .mapToInt(Cargo::getWeight)
                .sum();
    }
}
