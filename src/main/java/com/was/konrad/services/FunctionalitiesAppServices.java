package com.was.konrad.services;

import com.was.konrad.interfaces.FunctionalitiesAppServicesInterface;

public class FunctionalitiesAppServices implements FunctionalitiesAppServicesInterface {
    private final WeightService weightService;
    private final AirportService airportService;

    public FunctionalitiesAppServices(WeightService weightService, AirportService airportService) {
        this.weightService = weightService;
        this.airportService = airportService;
    }

    @Override
    public void weightOfTheCargo(int flightNumber, String departureDate) {
        int id = weightService.flightId(flightNumber, departureDate);
        int a = weightService.baggageWeightFotRequestFlight(id);
        System.out.println("Baggage Weight: " + a);
        int b = weightService.cargoWeightFotRequestFlight(id);
        System.out.println("Cargo Weight: " + b);
        int sum = a + b;
        System.out.println("Total weight: " + sum);
    }

    @Override
    public void arrivalsDeparturesAllLuggage(String iataCode, String date) {
        System.out.println("Number of departing planes: " + airportService.numberOfFlightDeparting(iataCode, date));
        System.out.println("Pieces of baggage departing from that airport: " + airportService.piecesOfBaggageDepartingFromThisAirport(iataCode, date));
        System.out.println("Number of arriving planes: " + airportService.numberOfFlightArriving(iataCode, date));
        System.out.println("Pieces of baggage arriving from this airport: " + airportService.numberOfBaggageArrivingToThisAirport(iataCode, date));
    }
}
