package com.was.konrad.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Load {
    private Integer flightId;
    private List<Baggage> baggage;
    private List<Cargo> cargo;
}