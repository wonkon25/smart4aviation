package com.was.konrad.interfaces;

public interface AirportServiceInterface {
    int numberOfFlightDeparting(String iataCode, String departureDate);

    int numberOfFlightArriving(String iataCode, String departureDate);

    int piecesOfBaggageDepartingFromThisAirport(String iataCode, String departureDate);

    int numberOfBaggageArrivingToThisAirport(String iataCode, String departureDate);
}
