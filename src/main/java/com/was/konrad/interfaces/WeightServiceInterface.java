package com.was.konrad.interfaces;

public interface WeightServiceInterface {
    int flightId(int flightNumber, String departureDate);

    int baggageWeightFotRequestFlight(int id);

    int cargoWeightFotRequestFlight(int id);
}
