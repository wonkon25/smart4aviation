package com.was.konrad.converter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.was.konrad.models.Flight;
import com.was.konrad.models.Load;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ConvertJsonToList {
    final static ObjectMapper mapper = new ObjectMapper();

    public static List<Flight> flightToList() {
        List<Flight> flightList = null;
        try {
            flightList = mapper.readValue(
                    new File("Flight Entity.json"),
                    new TypeReference<List<Flight>>() {
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return flightList;
    }

    public static List<Load> cargosToList() {
        List<Load> cargosList = null;
        try {
            cargosList = mapper.readValue(
                    new File("Cargo Entity.json"),
                    new TypeReference<List<Load>>() {
                    });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return cargosList;
    }
}
