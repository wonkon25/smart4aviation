package com.was.konrad;

import com.was.konrad.services.AirportService;
import com.was.konrad.services.FunctionalitiesAppServices;
import com.was.konrad.services.WeightService;

import java.util.NoSuchElementException;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        WeightService weightService = new WeightService();
        AirportService airportService = new AirportService();
        FunctionalitiesAppServices functionalitiesAppServices = new FunctionalitiesAppServices(weightService, airportService);
        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);

        try {
            System.out.println("First Functionality.");
            System.out.println("Enter date in the pattern yyyy-mm-dd");
            String departureDate = sc1.nextLine();
            System.out.println("Enter the plane number");
            int flightNumber = sc1.nextInt();

            functionalitiesAppServices.weightOfTheCargo(flightNumber, departureDate);
        } catch (NoSuchElementException exception) {
            System.out.println("Incorrect or missing data!");
        }

        System.out.println("Second Functionality.");
        System.out.println("Enter date in the pattern yyyy-mm-dd");
        String Date = sc2.nextLine();
        System.out.println("Enter the IATA Airport Code");
        String iataCode = sc2.nextLine();


        functionalitiesAppServices.arrivalsDeparturesAllLuggage(iataCode, Date);
    }
}
