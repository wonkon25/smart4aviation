package com.was.konrad.services;

import org.junit.Assert;
import org.junit.Test;

public class WeightServiceTest {
    WeightService weightService = new WeightService();

    @Test
    public void when_get_flight_number_and_departure_date_then_return_flight_id() {
        //given
        final int flightNumber = 5757;
        final String departureDate = "2016-04-27";
        //when
        final int result = weightService.flightId(flightNumber, departureDate);
        //then
        Assert.assertEquals(3, result);
    }

    @Test
    public void when_get_flight_id_then_return_weight_of_baggage() {
        //given
        final int flightId = 1;
        //when
        final int result = weightService.baggageWeightFotRequestFlight(flightId);
        //then
        Assert.assertEquals(3775, result);
    }

    @Test
    public void when_get_flight_id_then_return_weight_of_Cargo() {
        //given
        final int flightId = 2;
        //when
        final int result = weightService.cargoWeightFotRequestFlight(flightId);
        //then
        Assert.assertEquals( 1344, result);
    }
}